// we need socket.io on the client
var socket = io();

// create a new svg canvas
var s = Snap("#svg");


// instantiate new client attributes
var config = {
    name: 'User_' + (Math.random() * 10000).toFixed(0),
    color: randomColor({
        luminosity: 'dark',
        format: 'hex'
    }),
    transition: 'easein',
    size: 25,
};

let transitions = [
//        'backin',
//        'backout',
//        'elastic',
    'bounce',
    'linear',
    'easeout',
    'easein',
    'easeinout',
];

let messages = [],
    users = [];

rivets.binders['style-*'] = function (el, value) {
    el.style.setProperty(this.args[0], value);
};

rivets.bind(document.querySelector('#transitions'), {
    config,
    transitions,
});

let usersList = rivets.bind(document.querySelector('#usersList'), {
    users,
});

rivets.bind(document.querySelector('#messages'), {
    messages,
});

rivets.bind(document.querySelector('#nameField'), {
    config: config,
    changeName: (e, bind, g) => {
        socket.emit('name-change', bind.config.name);
    }
});

socket.emit('signin', config);

// listen for mouse movement and send an event with coordinates
document.addEventListener('mousemove', function (e) {

    const COORDS = {x: e.clientX, y: e.clientY};

    _.debounce(() => {
        socket.emit('mouse-move', COORDS, config);
    }, 200, {'maxWait': 500})();

    document.querySelector('body').style.cursor = 'none';


    _.debounce(showCursor, 2000, {'maxWait': 3500})();

    function showCursor() {
        document.querySelector('body').style.cursor = 'auto';
    }

});

socket.on('message', (msg, color) => {

    if (messages.length >= 5) {
        messages.splice(0, 1);
    }

    // check if this msg is equal to the msg at end of array, and only add if not the most recent message
    if (messages.map(d => d.text).lastIndexOf(msg) === -1 || messages.map(d => d.text).lastIndexOf(msg) !== messages.length - 1) {
        messages.push({
            datetime: new Date().toLocaleString(),
            text: msg,
            color: color,
        });
        adjustDescriptionHeight();
    } else {
        messages.pop();
        messages.push({
            datetime: new Date().toLocaleString(),
            text: msg,
            color: color,
        });
    }

});

socket.on('user-status-change', (newUsers) => {
    users = newUsers;
    usersList.update({users: users});
});

// when new screen paint event occurs, add an SVG animation
socket.on('screen paint', function (options) {

    let circle = s.circle(options.coords.x, options.coords.y, options.size);

    // set attributes for object
    circle.attr({
        r: 0,
        fill: options.color,
        stroke: 'rgba(240, 240, 240, 1)',
        strokeWidth: 3,
    });

    // animate the SVG object and remove from DOM after animation is complete
    circle.animate({r: options.size-5}, 1000, mina['easeout'], () => {

        circle.animate({r: 0, fill: 'white'}, 12000, mina['easein'], () => {
            circle.remove();
        });

    });


    let color = randomColor({
        luminosity: 'dark',
        format: 'rgb'
    });

    let hues = randomColor({
        count: 5,
        hue: randomColor({
            luminosity: 'dark',
            format: 'rgb'
        }),
    });

    hues.forEach(hue => {

        let explosion = s.circle(options.coords.x, options.coords.y, random(10, 50));

        explosion.attr({fill: hue,});

        let opts = {
            cx: Number(explosion.attr().cx) + random(-200, 200),
            cy: Number(explosion.attr().cy) + random(-200, 200),
            r: 0,
        };

        explosion.animate(opts, 3000, mina[options.transition], () => {
            explosion.remove();
        });

    });


    function random(min, max) {
        return Number(Math.floor(Math.random() * (max - min + 1)) + min);
    }

});

function adjustDescriptionHeight() {
    let el = document.querySelector('#description');
    el.style.height = window.innerHeight - el.offsetTop - (window.innerHeight - document.querySelector('#messages').offsetTop + 25);
}

window.addEventListener('resize', adjustDescriptionHeight);

adjustDescriptionHeight();