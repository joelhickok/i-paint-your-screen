var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var  _ = require('lodash');

app.use('/static', express.static('./static'));

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

let session = {};
let users = [];

io.on('connection', (socket) => {

    session[socket.id] = {};

    socket.on('signin', (config) => {
        session[socket.id] = config;
        io.emit('message', `Sign In From: ${config.name}`);
        users.push(config);
        io.emit('user-status-change',users);
    });

    socket.on('disconnect', () => {
        if (session[socket.id]) {

            let msg = `User disconnected: ${session[socket.id].name}`;
            io.emit('message', msg);

            let index = users.findIndex((el) => session[socket.id].name == el.name);
            users.splice(index, 1);
            io.emit('user-status-change', users);
        } else {
            console.log('Unknown user disconnected');
            io.emit('message', `Unknown user disconnected`);
        }
    });

    let sendMsg = _.debounce((msg, color) => io.emit('message', msg, color), 1000, {'leading': true, 'trailing': false});

    socket.on('mouse-move', (coords, config) => {

        let name = session[socket.id] ? session[socket.id].name : 'Unknown';

        sendMsg(`${name} is painting using the ${config.transition} transition.`, config.color);

        io.emit('screen paint', {
            coords: coords,
            // color: 'rgba(0,0,0,0)',
            color: config.color,
            fadeTime: 3000,
            name: config.name || 'Unknown User',
            size: config.size || 50,
            transition: config.transition || 'bounce',
        });
    });

    socket.on('name-change', (name) => {
        let msg = `New name for ${session[socket.id].name} is: ${name}`;
        io.emit('message', msg);

        session[socket.id].name = name;

        let index = users.findIndex((el) => session[socket.id].name === el.name);
        users[index] = session[socket.id];
        io.emit('user-status-change', users);
    });

});

http.listen(3333, () => {
    console.log('listening on *:3333');
});