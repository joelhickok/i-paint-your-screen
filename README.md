I Paint Your Screen!
=

This is a Node.js app put together to test a few libraries I've been wanting to play with a little.

1. Socket.io
2. Rivets.js
3. Snap.svg

The app runs a Node.js server to handle messaging, and delivers a static index to the client.  In our front-end app, we use the Socket.io library to play with mouse events.  

I am experimenting with Rivets.js as a new choice as a lightweight library for handling DOM binding and events when I do not need a full-fledged framework.  

Finally, Snap.svg provides a friendly API for animating SVG elements, although, I am feeling like the API for svg.js is more friendly for chaining animation steps.

<img src='./assets/demo.png' alt='Example image.' title='Example image.' width="600" />

Getting Started
-

### Clone repo

`git clone https://gitlab.com/joelhickok/i-paint-your-screen.git`

### Install Dependencies
`npm install`

### Run App
`node index`

### Run with Browser Sync (hot-reload)
`npm run dev`

... which is equivalent to running both of the following commands:

`node index`

 `browser-sync start --port 3333  --files '*.html, static/*.*, *.js' --no-ghost-mode`